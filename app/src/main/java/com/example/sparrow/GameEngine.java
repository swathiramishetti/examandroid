package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;
    int CAGE_WIDTH = 200;
   final int CAGE_SPEED = 20;

    Square bullett;



    Square enemy;

    Sprite player;
    Sprite sparrow;
    Sprite cat;
    Square cage;


    boolean movingLeft = true;
    boolean movingcatLeft = true;
    boolean movingDown = true;

    boolean youwin = false;



    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score = 0;
    public  float MouseX;
    public  float MouseY;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), 1500, 700, R.drawable.cat64);
        this.cage = new Square(context, 1500, 200, CAGE_WIDTH);
        this.bullett = new Square(context, 300, 700, SQUARE_WIDTH);


       // y = GamePanel.HEIGHT * Math.random();

    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }


    // Game Loop methods
    public void updateGame() {
        // y = GamePanel.HEIGHT * Math.random();
        if (movingLeft == true) {
               this.cage.setxPosition(this.cage.getxPosition() - 15);

        }
        else {
            this.cage.setxPosition(this.cage.getxPosition() + 15);
        }

        // @TODO: Collision detection code
        if (cage.getxPosition() > screenWidth - 200) {
            Log.d(TAG, "Racket reached right of screen. Changing direction!");
            movingLeft = true;
        }

        if (cage.getxPosition() < 0) {
            Log.d(TAG, "Racket reached left of screen. Changing direction!");
            movingLeft = false;
        }
        // make cat move
        if (movingcatLeft == true) {
            this.cat.setxPosition(this.cat.getxPosition() - 15);
            this.cat.updateHitbox();

        }
        else {
            this.cat.setxPosition(this.cat.getxPosition() + 15);
            this.cat.updateHitbox();

        }

        // @TODO: Collision detection code
        if (cat.getxPosition() > screenWidth - 200) {
            Log.d(TAG, "Racket reached right of screen. Changing direction!");
            movingcatLeft = true;
        }

        if (cat.getxPosition() < this.screenWidth/3) {
            Log.d(TAG, "Racket reached left of screen. Changing direction!");
            movingcatLeft = false;
        }



        Log.d(TAG,"Bullet position: " + this.bullett.getxPosition() + ", " + this.bullett.getyPosition());
        Log.d(TAG,"Enemy position: " + this.MouseX + ", " + this.MouseX);






        // 1. calculate distance between bullet and enemy
        double a = MouseX - this.bullett.getxPosition();
        double b = MouseY - this.bullett.getyPosition();

        // d = sqrt(a^2 + b^2)
        double d = Math.sqrt((a * a) + (b * b));

        Log.d(TAG, "Distance to enemy: " + d);

        // 2. calculate xn and yn constants
        // (amount of x to move, amount of y to move)
        double xn = (a / d);
        double yn = (b / d);

        // 3. calculate new (x,y) coordinates
        int newX = this.bullett.getxPosition() + (int) (xn * 15);
        int newY = this.bullett.getyPosition() + (int) (yn * 15);
        this.bullett.setxPosition(newX);
        this.bullett.setyPosition(newY);
        this.bullett.updateHitbox();
        this.cage.updateHitbox();

        Log.d(TAG,"----------");


        if (bullett.getHitbox().intersect(cage.getHitbox())) {

            this.cage.setyPosition(this.cage.getyPosition() + 40);



        }
        if (cage.getHitbox().intersect(cat.getHitbox())) {
            this.bullett.setxPosition(this.bullett.getxPosition() + 0);
            this.cat.setxPosition(this.cat.getxPosition() + 0);
            this.cage.setxPosition(0);



            youwin = true;




        }


    }




    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);


            // cat
            canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);

            // draw cage
            paintbrush.setColor(Color.BLACK);
            canvas.drawRect(
                    this.cage.getxPosition(),
                    this.cage.getyPosition(),
                    this.cage.getxPosition() + this.cage.getWidth(),
                    this.cage.getyPosition() + this.cage.getWidth(),
                    paintbrush
            );

// draw bullet
            // draw bullet
            paintbrush.setColor(Color.BLACK);
            canvas.drawRect(
                    this.bullett.getxPosition(),
                    this.bullett.getyPosition(),
                    this.bullett.getxPosition() + this.bullett.getWidth(),
                    this.bullett.getyPosition() + this.bullett.getWidth(),
                    paintbrush
            );



            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);


            paintbrush.setColor(Color.RED);
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(
                    this.bullett.getHitbox(),
                    paintbrush
            );
            paintbrush.setColor(Color.BLUE);
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(
                    this.cage.getHitbox(),
                    paintbrush
            );
            paintbrush.setColor(Color.RED);
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(
                    this.cat.getHitbox(),
                    paintbrush
            );
            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);

            if (youwin == true) {
                paintbrush.setColor(Color.BLUE);

                canvas.drawText("you win!", screenWidth/2, screenHeight/2, paintbrush);
            }

            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "The person tapped: (" + event.getX() + "," + event.getY() + ")");

                MouseX = event.getX();
                MouseY = event.getY();
                System.out.println("(" + MouseX + ", " + MouseY + ")");

                break;
            case MotionEvent.ACTION_DOWN:
                break;
       }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

